package edu.upc.damo;

import android.app.Activity;
import android.widget.TextView;

/**
 * Created by Josep M on 09/10/2014.
 * <p/>
 * Responsable de la presentació
 * Observa el model i s'actualitza quanaquest l'avisa
 */
public class Vista {

    private TextView res;

    public Vista(Activity a) {
        res = (TextView) a.findViewById(R.id.resultat);

    }


    public void refesPresentacio(ModelObservable model) {
        if (model==null) return;

        res.setText("");

        int i = 1;
        for (CharSequence c : model) {
            res.append(String.valueOf(i));
            res.append(" ");
            res.append(c);
            res.append("\n");
            i++;
        }
    }


}
